package com.target.myretail;

import java.util.Iterator;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

public class MongoDAO {





	public MongoDatabase createMongoConnection(){
		MongoClient mongo = new MongoClient("localhost", 27017);

		// Creating Credentials
		//MongoCredential credential;
		//credential = MongoCredential.createCredential("sampleUser", "myDb", "password".toCharArray());
		System.out.println("Connected to the database successfully");

		// Accessing the database
		MongoDatabase database = mongo.getDatabase("myDb");
		if(database == null){
			System.out.println("Database connection problem");
		}
		return database;

	}


	public void persistProduct(ProductPojo p) {

		// Retrieving a collection
		MongoCollection<Document> collection = createMongoConnection().getCollection("products");
		if(collection == null)
		{
			System.out.println("Collection not found!!");
		}
		else
		{
			System.out.println("Collection sampleCollection selected successfully");
		}


		Document document = new Document("name", p.getName())
				.append("id", p.getId())
				.append("price", p.getPrice());
		collection.insertOne(document);
		System.out.println("Document inserted successfully");
	}



	public ProductPojo getProductDetails(int id){

		ProductPojo p = new ProductPojo();
		
		// Retrieving a collection 
		MongoCollection<Document> collection = createMongoConnection().getCollection("products");

		if(collection == null){
			System.out.println("Collection not found");
		}
		else{
			System.out.println("Collection sampleCollection selected successfully"); 
		}
		
		BasicDBObject ob = new BasicDBObject();
		ob.put("id", id);
		
		FindIterable<Document> fi = collection.find(ob);
		Document d = fi.first();
		p.setId(Integer.parseInt(d.get("id").toString()));
		p.setName( d.get("name").toString());
		p.setPrice(d.get("price").toString());
		return p;
	}
	
	
	public void updateProductDetails(ProductPojo p){
		
		MongoCollection<Document> collection = createMongoConnection().getCollection("products");
	    
		if(collection == null){
			System.out.println("Collection not found");
		}else{
			System.out.println("Collection found selected successfully"); 
		}
		
		BasicDBObject ob = new BasicDBObject();
		ob.put("id", p.getId());
		
		  collection.updateOne(Filters.eq("id", p.getId()), Updates.set("name", p.getName()));      
		  collection.updateOne(Filters.eq("id", p.getId()), Updates.set("price", p.getPrice()));
	      System.out.println("Document update successfully...");  
	      
	      // Retrieving the documents after updation 
	      // Getting the iterable object
	}
	
}
