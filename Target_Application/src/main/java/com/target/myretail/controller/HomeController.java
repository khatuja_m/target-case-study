package com.target.myretail.controller;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.target.myretail.dao.ProductDAO;
import com.target.myretail.model.Product;

@RestController
public class HomeController {

	ProductDAO productDao = new ProductDAO();

	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
	public ResponseEntity getProdcutById(@PathVariable("id") String key) throws JsonProcessingException {
		System.out.println("I 'm here!!" + key);

		Product p = productDao.getProductDetails(Integer.parseInt(key));

		ObjectMapper om = new ObjectMapper();
		String results = om.writeValueAsString(p);

		// System.out.println(p.getName()+p.getId()+p.getPrice());

		return new ResponseEntity<String>(results, HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/product", method = RequestMethod.POST)
	public ResponseEntity insertProduct(@RequestBody String str)
			throws JsonParseException, JsonMappingException, IOException {
		System.out.println("I 'm here in post method!!" + str);

		ObjectMapper om = new ObjectMapper();
		JsonNode node = om.readValue(str, JsonNode.class);

		Product pj = new Product();

		pj.setId(Integer.parseInt(node.get("id").toString()));
		pj.setName(node.get("name").asText());
		pj.setPrice(node.get("price").asText());

		System.out.println(pj.getName() + pj.getPrice());

		boolean inserted = productDao.persistProduct(pj);
		if (inserted)
			return new ResponseEntity<String>("Successfully Inserted the Product", HttpStatus.ACCEPTED);
		else
			return new ResponseEntity<String>("Product already exists", HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/product/{id}", method = RequestMethod.PUT)
	public ResponseEntity UpdateProduct(@PathVariable("id") String key, @RequestBody String str)
			throws JsonParseException, JsonMappingException, IOException {
		System.out.println("I 'm here in put method!!" + str);

		Product p = productDao.getProductDetails(Integer.parseInt(key));

		ObjectMapper om = new ObjectMapper();
		JsonNode node = om.readValue(str, JsonNode.class);

		
		p.setName(node.get("name").asText());
		p.setPrice(node.get("price").asText());

		System.out.println(p.getName() + p.getPrice());

		boolean updated = productDao.updateProductDetails(p);
		
		if (updated)
		return new ResponseEntity<String>("Successfully Updated the Product", HttpStatus.ACCEPTED);
		
		else
			return new ResponseEntity<String>("Product doesn't exists", HttpStatus.BAD_REQUEST);
	}

	@RequestMapping("/welcomeone")
	public String starerOne() {
		System.out.println("I 'm here!!");

		return "welcome";
	}
}
