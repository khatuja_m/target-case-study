package com.target.myretail.dao;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class DAO {
	private static DAO dao = null;
	private MongoDatabase database;
	
	public static DAO getInstance() {
		if (dao == null) {
			dao = new DAO();
		}
		return dao;
	}

	private DAO() {
		// TODO Auto-generated constructor stub
		createMongoConnection();
	}

	private void createMongoConnection() {

		System.out.println("Connected to the database successfully");
		MongoClient mongo = new MongoClient();
		// Accessing the database
		database = mongo.getDatabase("myDb");
		if (database == null) {
			System.out.println("Database connection problem");
		}
	}

	public MongoDatabase getDatabase() {
		return database;
	}

	public void setDatabase(MongoDatabase database) {
		this.database = database;
	}
	
}
