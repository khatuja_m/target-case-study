package com.target.myretail.dao;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.target.myretail.model.Product;

public class ProductDAO {

	private DAO dao;
	private MongoCollection<Document> collection;

	public ProductDAO() {
		dao = DAO.getInstance();
		// Retrieving a collection
		collection = dao.getDatabase().getCollection("products");
	}

	public boolean persistProduct(Product p) {

		if (collection == null) {
			System.out.println("Collection not found!!");
		} else {
			System.out.println("Collection sampleCollection selected successfully");
		}
		Product dbP = getProductDetails(p.getId());
		if (dbP == null) {
			Document document = new Document("name", p.getName()).append("id", p.getId()).append("price", p.getPrice());
			collection.insertOne(document);
			System.out.println("Document inserted successfully");
			return true;
		} else {
			System.out.println("Product already exists");
			return false;
		}
	}

	public Product getProductDetails(int id) {

		Product p = new Product();

		if (collection == null) {
			System.out.println("Collection not found");
		} else {
			System.out.println("Collection sampleCollection selected successfully");
		}

		BasicDBObject ob = new BasicDBObject();
		ob.put("id", id);

		FindIterable<Document> fi = collection.find(ob);

		Document d = fi.first();
		if (d != null) {
			p.setId(Integer.parseInt(d.get("id").toString()));
			p.setName(d.get("name").toString());
			p.setPrice(d.get("price").toString());

			return p;
		} else {
			return null;
		}
	}

	public boolean updateProductDetails(Product p) {

		if (collection == null) {
			System.out.println("Collection not found");
		} else {
			System.out.println("Collection found selected successfully");
		}

		BasicDBObject ob = new BasicDBObject();
		ob.put("id", p.getId());

		Product dbP = getProductDetails(p.getId());
		if (dbP != null) {
			collection.updateOne(Filters.eq("id", p.getId()), Updates.set("name", p.getName()));
			collection.updateOne(Filters.eq("id", p.getId()), Updates.set("price", p.getPrice()));
			System.out.println("Document update successfully...");
			return true;
		} else {
			return false;
		}
	}

}
